/**
 * @author: Anton Shulgin
 */

const webpack = require('webpack');
const helpers = require('./helpers');

/**
 * Webpack plugins
 */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = function(options) {
    let isProd = options.env === 'production';

    return {
        entry: {
            'polyfills': './src/polyfills.ts',
            'vendor': './src/vendor.ts',
            'main': './src/main.ts'
        },
        resolve: {
            extensions: ['.ts', '.js', '.json'],
            modules: [
                helpers.root('src'),
                helpers.root('node_modules')
            ]
        },
        module: {
            rules: [
                {
                    test: /\.json$/,
                    use: 'json-loader'
                },
                {
                    test: /\.ts$/,
                    use: [
                        {
                            loader: 'awesome-typescript-loader',
                            options: {
                                configFileName: 'tsconfig.webpack.json',
                                useCache: !isProd
                            }
                        },
                        {
                            loader: 'angular2-template-loader'
                        }
                    ]
                },
                {
                    test: /\.html$/,
                    use: ['html-loader'],
                    exclude: [
                        helpers.root('src/index.html')
                    ]
                },
                {
                    test: /\.html$/,
                    use: ['raw-loader'],
                    include: [
                        helpers.root('src/index.html')
                    ]
                },
                {
                    test: /\.css$/,
                    use: ['to-string-loader', 'css-loader'],
                    exclude: [
                        helpers.root('src', 'styles')
                    ]
                },
                {
                    test: /\.styl$/,
                    use: ['to-string-loader', 'css-loader', 'stylus-loader'],
                    exclude: [
                        helpers.root('src', 'styles')
                    ]
                },
                {
                    test: /\.(jpg|jpeg|png|gif)$/,
                    use: 'file-loader'
                },
                {
                    test: /\.(eot|woff2?|svg|ttf)$/,
                    use: 'file-loader'
                }
            ]
        },
        plugins: [
            new webpack.ContextReplacementPlugin(
                // The (\\|\/) piece accounts for path separators in *nix and Windows
                /angular(\\|\/)core(\\|\/)@angular/,
                helpers.root('./src'), // location of your src
                {} // a map of your routes
            ),

            new HtmlWebpackPlugin({
                template: 'src/index.html',
                chunksSortMode: 'dependency',
                inject: 'body'
            }),
            
            // Workaround for angular/angular#11580
            new webpack.optimize.CommonsChunkPlugin({
                name: ['app', 'vendor', 'polyfills']
            }),

            new webpack.DefinePlugin({
                'ENV': JSON.stringify(options.env),
                'process.env': {
                    'ENV': JSON.stringify(options.env),
                    'NODE_ENV': JSON.stringify(options.env),
                }
            }),

            new webpack.ProvidePlugin({
                '$': 'jquery',
                'jquery': 'jquery',
                'jQuery': 'jquery',
            }),

            new CopyWebpackPlugin(
                [
                    { from: 'src/assets', to: 'assets' }
                ],
                (isProd) ? { ignore: [ 'mock-data/**/*' ] } : undefined
            )
        ]
    };
}