export interface ITextInputSelection {
    start: number;
    end: number;
}

export function getInputTextSelection(el: HTMLTextAreaElement): ITextInputSelection {
    let start: number = 0;
    let end: number = 0;

    if (typeof el.selectionStart === "number" && typeof el.selectionEnd === "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        const range = document["selection"].createRange();

        if (range && range.parentElement() === el) {
            const len: number = el.value.length;
            const normalizedValue: string = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            const textInputRange = el["createTextRange"]();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            const endRange = el["createTextRange"]();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {start, end};
}
