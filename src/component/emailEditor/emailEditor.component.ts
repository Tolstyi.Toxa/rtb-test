import {AfterViewChecked, AfterViewInit, Component, ElementRef, OnDestroy, ViewChild} from "@angular/core";
import {getInputTextSelection, ITextInputSelection} from "../../utils/input/getInputTextSelection";

export interface IContactItem {
    email: string;
    isValid: boolean;
}

@Component({
    selector: "as-email-editor",
    styleUrls: ["./emailEditor.component.styl"],
    templateUrl: "./emailEditor.component.html"
})
export class EmailEditorComponent implements OnDestroy, AfterViewInit, AfterViewChecked {
    @ViewChild("hiddenInput")
    public hiddenInput: ElementRef;
    @ViewChild("input")
    public input: ElementRef;
    @ViewChild("editor")
    public editor: ElementRef;
    public contacts: IContactItem[] = [];
    public selectedIndex: number = -1;
    public width: number;
    private emailRegexp: RegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    private checkSizeInterval: number;
    private $host: JQuery;
    private _prevContactsCount: number = this.contacts.length;

    constructor(elementRef: ElementRef) {
        this.$host = $(elementRef.nativeElement);
    }

    public ngAfterViewInit(): void {
        this.checkSizeInterval = window.setInterval(() => {
            const currentParentWidth: number = this.$host.parent().innerWidth();
            if (this.width !== currentParentWidth && currentParentWidth) {
                $(this.editor.nativeElement)
                    .width(currentParentWidth - (this.$host.outerWidth() - this.$host.innerWidth()));
                this.width = currentParentWidth;
            }
        }, 500);
    }

    public ngAfterViewChecked(): void {
        if (this._prevContactsCount !== this.contacts.length) {
            this._prevContactsCount = this.contacts.length;
            this.resetTextAreaWidth();
        }
    }

    public ngOnDestroy(): void {
        if (this.checkSizeInterval) {
            window.clearInterval(this.checkSizeInterval);
        }
    }

    public onTextAreaKeyUp(event: KeyboardEvent): void {
        this.maximizeTextAreaWidth();
    }

    public onTextAreaKeyDown(event: KeyboardEvent): boolean {
        let pos: ITextInputSelection;
        switch (event.keyCode) {
            // Кнопка влево
            case 37:
                pos = getInputTextSelection(this.getTextAreaHtmlElement());
                if (pos.start === 0 && this.contacts.length) {
                    this.selectedIndex = this.contacts.length - 1;
                    this.getHiddenInputHtmlElement().focus();
                }
                break;
            // Enter, запятая
            case 13:
            case 188:
                this.defaultAddContacts();
                event.stopImmediatePropagation();
                event.stopPropagation();
                event.preventDefault();
                return false;
            // Backspace
            case 8:
                pos = getInputTextSelection(this.getTextAreaHtmlElement());

                if (pos.start === 0 && pos.start === pos.end && this.contacts.length > 0) {
                    this.contacts.splice(this.contacts.length - 1, 1);
                }
                break;
            // Пробел
            case 32:
                // Если только пробелы во вводе - ничего не делаем
                if (!this.getTextAreaValue()) {
                    event.stopImmediatePropagation();
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                }
                break;
        }
        return true;
    }

    public onTextAreaPaste(event: ClipboardEvent): boolean {
        const emails: string[] = this.getValues(event.clipboardData.getData("text/plain"));

        if (!emails.length) {
            return true;
        }

        const email: string = emails.splice(emails.length - 1, 1)[0];
        this.addContacts(emails);
        this.setTextAreaValue(email);
        event.stopImmediatePropagation();
        event.stopPropagation();
        event.preventDefault();
        return false;
    }

    public onTextAreaBlur(event: Event): void {
        this.defaultAddContacts();
    }

    public onHiddenInputKeyDown(event: KeyboardEvent): boolean {
        switch (event.keyCode) {
            case 37:
                this.selectedIndex = Math.max(0, this.selectedIndex - 1);
                break;
            case 39:
                if (this.selectedIndex + 1 === this.contacts.length) {
                    this.getTextAreaHtmlElement().focus();
                } else {
                    this.selectedIndex++;
                }
                break;
            case 8:
            case 46:
                this.removeContact(this.selectedIndex);
                if (this.contacts.length === this.selectedIndex) {
                    this.getTextAreaHtmlElement().focus();
                }
                break;
        }
        event.stopImmediatePropagation();
        event.stopPropagation();
        event.preventDefault();
        return false;
    }

    public onHiddenInputBlur(event: KeyboardEvent): void {
        this.selectedIndex = -1;
        this.getHiddenInputHtmlElement().value = "";
    }

    public onRemoveContact(event: Event, index: number): void {
        if (index >= 0 && index < this.contacts.length) {
            if (index === this.selectedIndex) {
                this.selectedIndex = -1;
            }
            this.removeContact(index);
        }
    }

    public onContactItemClick(event: Event, index: number): void {
        if (index >= 0 && index < this.contacts.length) {
            this.selectedIndex = index;
            this.getHiddenInputHtmlElement().focus();
        }
    }

    public getContactsCount(): number {
        return this.contacts.length;
    }

    public addContact(email: string): number {
        const emails: string[] = this.getValues(email);
        return this.addContacts(emails);
    }

    private maximizeTextAreaWidth(): void {
        if (this.isTextAreaMultiLine()) {
            const width: number = Math.max(this.width - 35, (this.contacts.length) ? 180 : 250);
            $(this.input.nativeElement).css({width});
        }
    }

    private smallingTextAreaWidth(del: boolean = false): void {
        $(this.input.nativeElement).css({
            width: (this.contacts.length - ((del) ? 1 : 0)) ? 180 : 250
        });
    }

    private resetTextAreaWidth(): void {
        let width: number;

        if (!this.contacts.length) {
            width = Math.max(this.width - 35, (this.contacts.length) ? 180 : 250);
        } else {
            const clientRectLastContactItem: ClientRect = (
                    $(this.editor.nativeElement).find("as-contact-item:last")
                )[0]
                .getBoundingClientRect();
            const clientRectContainer: ClientRect = $(this.input.nativeElement)
                .parent()[0]
                .getBoundingClientRect();

            width = clientRectContainer.right - clientRectLastContactItem.right - 35;
            if (width < 180) {
                width = this.width - 35;
            }
        }
        $(this.input.nativeElement).css({width});
    }

    private removeContact(index: number): void {
        this.smallingTextAreaWidth(true);
        this.contacts.splice(index, 1);
    }

    private defaultAddContacts(): void {
        const emails: string[] = this.getValues(this.getTextAreaValue());
        this.addContacts(emails);
        this.clearTextAreaValue();
    }

    private _addContact(email: string): boolean {
        if (this.hasItemByEmail(email)) {
            return false;
        }

        this.contacts.push({
            email: email,
            isValid: this.checkEmail(email)
        });

        return true;
    }

    private addContacts(emails: string[]): number {
        this.smallingTextAreaWidth();
        let count: number = 0;
        for (const email of emails) {
            if (this._addContact(email)) {
                ++count;
            }
        }
        return count;
    }

    private hasItemByEmail(email: string): boolean {
        for (const contact of this.contacts) {
            if (contact.email === email) {
                return true;
            }
        }

        return false;
    }

    private getTextAreaHtmlElement(): HTMLTextAreaElement {
        return this.input.nativeElement as HTMLTextAreaElement;
    }

    private getHiddenInputHtmlElement(): HTMLInputElement {
        return this.hiddenInput.nativeElement as HTMLInputElement;
    }

    private getTextAreaValue(): string {
        return ((this.getTextAreaHtmlElement().value) || "").trim();
    }

    private clearTextAreaValue(): void {
        this.setTextAreaValue("");
    }

    private setTextAreaValue(value: string): void {
        (this.input.nativeElement as HTMLTextAreaElement).value = value;
    }

    private getValues(value: string): string[] {
        const values: string[] = value
            .replace(/(\n\r|\r|\n|,)/g, "\n")
            .split("\n")
            .map((x: string) => (x || "").trim().replace(/\s+/g, " "))
            .filter((x: string) => !!x);

        return values;
    }

    private checkEmail(text: string): boolean {
        return this.emailRegexp.test(text);
    }

    private isTextAreaMultiLine(): boolean {
        return $(this.input.nativeElement).outerHeight() < $(this.input.nativeElement).prop("scrollHeight");
    }
}
