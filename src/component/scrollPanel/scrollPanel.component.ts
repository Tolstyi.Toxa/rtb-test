// https://puchk.in/balalayka/ui/#!/components/scroll
import {AfterViewInit, Component, ElementRef, HostListener, OnDestroy, ViewChild} from "@angular/core";

@Component({
    selector: "as-scroll-panel",
    styleUrls: ["./scrollPanel.component.styl"],
    templateUrl: "./scrollPanel.component.html"
})
export class ScrollPanelComponent implements AfterViewInit, OnDestroy {
    public nativeElement: HTMLElement;
    private oldWidth: number;
    private oldContentWidth: number;
    private checkWidthContentChangeInterval: number;
    private oldHeight: number;
    private oldContentHeight: number;
    private checkHeightContentChangeInterval: number;

    private $nativeElement: JQuery;
    @ViewChild("main")
    private main: ElementRef;
    private _$main: JQuery;
    private get $main(): JQuery {
        if (!this._$main) {
            this._$main = $(this.main.nativeElement);
        }
        return this._$main;
    }
    @ViewChild("inner")
    private inner: ElementRef;
    private _$inner: JQuery;
    private get $inner(): JQuery {
        if (!this._$inner) {
            this._$inner = $(this.inner.nativeElement);
        }
        return this._$inner;
    }

    @ViewChild("vtrack")
    private vTrack: ElementRef;
    private _$vTrack: JQuery;
    private get $vTrack(): JQuery {
        if (!this._$vTrack) {
            this._$vTrack = $(this.vTrack.nativeElement);
        }
        return this._$vTrack;
    }
    @ViewChild("vnativedrag")
    private vNativeDrag: ElementRef;
    private _$vNativeDrag: JQuery;
    private get $vNativeDrag(): JQuery {
        if (!this._$vNativeDrag) {
            this._$vNativeDrag = $(this.vNativeDrag.nativeElement);
        }
        return this._$vNativeDrag;
    }
    @ViewChild("vnativedragslyder")
    private vNativeDragSlyder: ElementRef;
    private _$vNativeDragSlyder: JQuery;
    private get $vNativeDragSlyder(): JQuery {
        if (!this._$vNativeDragSlyder) {
            this._$vNativeDragSlyder = $(this.vNativeDragSlyder.nativeElement);
        }
        return this._$vNativeDragSlyder;
    }
    @ViewChild("vdrag")
    private vDrag: ElementRef;
    private _$vDrag: JQuery;
    private get $vDrag(): JQuery {
        if (!this._$vDrag) {
            this._$vDrag = $(this.vDrag.nativeElement);
        }
        return this._$vDrag;
    }

    @ViewChild("htrack")
    private hTrack: ElementRef;
    private _$hTrack: JQuery;
    private get $hTrack(): JQuery {
        if (!this._$hTrack) {
            this._$hTrack = $(this.hTrack.nativeElement);
        }
        return this._$hTrack;
    }
    @ViewChild("hnativedrag")
    private hNativeDrag: ElementRef;
    private _$hNativeDrag: JQuery;
    private get $hNativeDrag(): JQuery {
        if (!this._$hNativeDrag) {
            this._$hNativeDrag = $(this.hNativeDrag.nativeElement);
        }
        return this._$hNativeDrag;
    }
    @ViewChild("hnativedragslyder")
    private hNativeDragSlyder: ElementRef;
    private _$hNativeDragSlyder: JQuery;
    private get $hNativeDragSlyder(): JQuery {
        if (!this._$hNativeDragSlyder) {
            this._$hNativeDragSlyder = $(this.hNativeDragSlyder.nativeElement);
        }
        return this._$hNativeDragSlyder;
    }
    @ViewChild("hdrag")
    private hDrag: ElementRef;
    private _$hDrag: JQuery;
    private get $hDrag(): JQuery {
        if (!this._$hDrag) {
            this._$hDrag = $(this.hDrag.nativeElement);
        }
        return this._$hDrag;
    }

    constructor(private host: ElementRef) {
        this.$nativeElement = $(this.host.nativeElement);
        this.nativeElement = this.host.nativeElement as HTMLElement;
    }

    public ngOnDestroy(): void {
        if (this.checkWidthContentChangeInterval) {
            clearInterval(this.checkWidthContentChangeInterval);
        }
        if (this.checkHeightContentChangeInterval) {
            clearInterval(this.checkHeightContentChangeInterval);
        }
    }

    public ngAfterViewInit(): void {
        this.initHorizontalScroll();
        this.initVerticalScroll();
        this.drawScroll();

        // Переинициализируем при ресайзе вложенного контейнера по вертикали
        this.oldContentHeight = this.$inner.height();
        this.oldHeight = this.$nativeElement.height();
        this.checkHeightContentChangeInterval = window.setInterval(
            () => {
                const contentHeight: number = this.$inner.height();
                const height: number = this.$nativeElement.height();
                if (
                    this.oldContentHeight !== contentHeight ||
                    this.oldHeight !== height
                ) {
                    this.drawVerticalScroll();
                    this.oldContentHeight = contentHeight;
                    this.oldHeight = height;
                }
            }, 500
        );

        // Переинициализируем при ресайзе вложенного контейнера по горизонтали
        this.oldContentWidth = this.$inner.width();
        this.oldWidth = this.$nativeElement.width();
        this.checkWidthContentChangeInterval = window.setInterval(
            () => {
                const contentWidth: number = this.$inner.width();
                const width: number = this.$nativeElement.width();
                if (
                    this.oldContentWidth  !== contentWidth ||
                    this.oldWidth !== width
                ) {
                    this.drawHorizontalScroll();
                    this.oldContentWidth = contentWidth;
                    this.oldWidth = width;
                }
            }, 500
        );
    }

    private initHorizontalScroll(): void {
        // При скролле колёсиком перемещаем «таскалку»
        this.$main.scroll(() => {
            this.$hDrag.css({
                left: this.$hTrack.width() / (-1 * (this.$inner.width() / this.$inner.position().left)) + 1
            });
            this.$nativeElement.addClass("now_scrolling");
        });

        this.$main.on("scrollstop", () => {
            this.$hNativeDrag.scrollLeft(this.$main.scrollLeft());
            this.$nativeElement.removeClass("now_scrolling");
        });

        // При скролле с нажатием клавишы мышки перемещаем «таскалку»
        this.$hNativeDrag.scroll(() => {
            this.$main.scrollLeft(this.$hNativeDrag.scrollLeft());
        });
    }

    private drawHorizontalScroll(): void {
        const innerWidth: number = this.$inner.width();
        const hTrackWidth: number = this.$hTrack.width();
        if (innerWidth <= this.$main.width()) {
            this.$hTrack.hide();
            return;
        }

        this.$hTrack.show();

        // Задаём высоту вертикальной «таскалке»
        this.$hDrag.css({
            width: hTrackWidth / (innerWidth / hTrackWidth)
        });

        // Задаём высоту содержимого нативному вспомогательному скроллу
        this.$hNativeDragSlyder.css({
            width: innerWidth
        });
    }

    private initVerticalScroll(): void {
        // При скролле колёсиком перемещаем «таскалку»
        this.$main.scroll(() => {
            this.$vDrag.css({
                top: this.$vTrack.height() / (-1 * (this.$inner.height() / this.$inner.position().top)) + 1
            });
            this.$nativeElement.addClass("now_scrolling");
        });
        this.$main.on("scrollstop", () => {
            this.$vNativeDrag.scrollTop(this.$main.scrollTop());
            this.$nativeElement.removeClass("now_scrolling");
        });

        // При скролле с нажатием клавишы мышки перемещаем «таскалку»
        this.$vNativeDrag.scroll(() => {
            this.$main.scrollTop(this.$vNativeDrag.scrollTop());
        });
    }

    private drawVerticalScroll(): void {
        const innerHeight: number = this.$inner.height();
        const vTrackHeight: number = this.$vTrack.height();
        if (innerHeight <= this.$main.height()) {
            this.$vTrack.hide();
            return;
        }

        this.$vTrack.show();

        // Задаём ширину горизонтальной «таскалке»
        this.$vDrag.css({
            height: vTrackHeight / (innerHeight / vTrackHeight)
        });

        // Задаём высоту содержимого нативному вспомогательному скроллу
        this.$vNativeDragSlyder.css({
            height: innerHeight
        });
    }

    private drawScroll(): void {
        this.drawHorizontalScroll();
        this.drawVerticalScroll();
    }

    @HostListener("window:resize", ["$event"])
    private onWindowResize(event: Event): void {
        this.drawScroll();
    }
}
