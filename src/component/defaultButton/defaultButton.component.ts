import {Component} from "@angular/core";
import {ButtonComponent} from "../button/button.component";

@Component({
    selector: "as-default-button",
    styleUrls: [
        "../button/button.component.styl",
        "./defaultButton.component.styl"
    ],
    templateUrl: "../button/button.component.html"
})
export class DefaultButtonComponent extends ButtonComponent {
    constructor() {
        super();
        this.className = "default-button";
        this.disabledClassName = "default-button-disabled";
        this.hoveredClassName = "default-button-hovered";
        this.outlineClassName = "default-button-outline";
    }
}
