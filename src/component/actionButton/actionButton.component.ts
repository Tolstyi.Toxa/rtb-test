import {Component} from "@angular/core";
import {ButtonComponent} from "../button/button.component";

@Component({
    selector: "as-action-button",
    styleUrls: [
        "../button/button.component.styl",
        "./actionButton.component.styl"
    ],
    templateUrl: "../button/button.component.html"
})
export class ActionButtonComponent extends ButtonComponent {
    constructor() {
        super();
        this.className = "action-button";
        this.disabledClassName = "action-button-disabled";
        this.hoveredClassName = "action-button-hovered";
        this.outlineClassName = "action-button-outline";
    }
}
