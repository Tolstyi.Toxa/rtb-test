import {Component, ElementRef, EventEmitter, HostListener, Input, Output} from "@angular/core";
import {IContactItem} from "../emailEditor/emailEditor.component";

@Component({
    selector: "as-contact-item",
    styleUrls: ["./contactItem.component.styl"],
    templateUrl: "./contactItem.component.html"
})
export class ContactItemComponent {
    @Input("contact")
    public set contact(value: IContactItem) {
        this._contact = value;
        if (this._contact.isValid) {
            this.$host.removeClass("warning");
        } else {
            this.$host.addClass("warning");
        }
    }
    public get contact(): IContactItem {
        return this._contact;
    }
    @Input("width")
    public width: number;
    @Input("selected")
    public set selected(value: boolean) {
        this._selected = value;
        if (this._selected) {
            this.$host.addClass("selected");
        } else {
            this.$host.removeClass("selected");
        }
    }
    public get selected(): boolean {
        return this._selected;
    }
    @Output("remove")
    public onRemove: EventEmitter<Event> = new EventEmitter<Event>();
    private _selected: boolean;
    private _contact: IContactItem;
    private $host: JQuery;

    public constructor(elementRef: ElementRef) {
        this.$host = $(elementRef.nativeElement);
    }

    public remove(event: Event): void {
        this.onRemove.emit(event);
    }

    @HostListener("mouseenter", ["$event"])
    private onMouseEnter(event: MouseEvent): void {
        this.$host.addClass("hovered");
    }

    @HostListener("mouseleave", ["$event"])
    private onMouseLeave(event: MouseEvent): void {
        this.$host.removeClass("hovered");
    }
}
