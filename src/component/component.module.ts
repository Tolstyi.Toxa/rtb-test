import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {ActionButtonComponent} from "./actionButton/actionButton.component";
import {ContactItemComponent} from "./contactItem/contactItem.component";
import {DefaultButtonComponent} from "./defaultButton/defaultButton.component";
import {EmailEditorComponent} from "./emailEditor/emailEditor.component";
import {ScrollPanelComponent} from "./scrollPanel/scrollPanel.component";

@NgModule({
    declarations: [
        ActionButtonComponent,
        ContactItemComponent,
        DefaultButtonComponent,
        EmailEditorComponent,
        ScrollPanelComponent
    ],
    exports: [
        ActionButtonComponent,
        DefaultButtonComponent,
        EmailEditorComponent,
        ScrollPanelComponent
    ],
    imports: [
        CommonModule,
        BrowserModule
    ]
})
export class ComponentModule {
}
