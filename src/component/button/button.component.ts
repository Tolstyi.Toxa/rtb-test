import {EventEmitter, HostListener, Input, Output} from "@angular/core";

export abstract class ButtonComponent {
    @Input("disabled")
    public disabled: boolean = false;
    @Input("text")
    public text: string = "";
    public className: string = "";
    public disabledClassName: string = "";
    public hoveredClassName: string = "";
    public outlineClassName: string = "";
    @Output()
    public mouseClick: EventEmitter<Event> = new EventEmitter();
    private hovered: boolean = false;

    public onClick(event: Event): void {
        if (!this.disabled) {
            this.mouseClick.emit(event);
        }
    }

    @HostListener("mouseenter", ["$event"])
    private onMouseEnter(event: Event): void {
        this.hovered = true;
    }

    @HostListener("mouseleave", ["$event"])
    private onMouseLeave(event: Event): void {
        this.hovered = false;
    }
}
