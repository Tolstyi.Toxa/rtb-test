import {Component, ElementRef, HostListener, Input, ViewChild} from "@angular/core";
import {ScrollPanelComponent} from "../../component/scrollPanel/scrollPanel.component"

interface IDialogMouseMove {
    inMove: boolean;
    x: number;
    y: number;
}

@Component({
    selector: "as-modal-dialog-component",
    styleUrls: ["./modal.dialog.component.styl"],
    templateUrl: "./modal.dialog.component.html"
})
export class ModalDialogComponent {
    public display: boolean = false;
    public visible: boolean = false;
    public visibleAnimate: boolean = false;
    public left: number = 0;
    public top: number = 0;
    @ViewChild("dialog")
    public dialog: ScrollPanelComponent;
    @Input("close-on-click")
    public closeOnClick: boolean = false;
    @Input("custom-class")
    public customClass: string;
    @Input("change-position")
    public changePosition: boolean = false;
    private showDelay: number = 100;
    private hideDelay: number = 300;
    private showTimeout: number;
    private hideTimeout: number;
    private mouseMoveData: IDialogMouseMove = null;

    public show(): void {
        this.top = 0;
        this.left = 0;
        this.display = true;
        if (this.hideTimeout) {
            clearTimeout(this.hideTimeout);
        }
        this.showTimeout = setTimeout(
            () => {
                this.visibleAnimate = true;
                this.visible = true;
                this.setLeftTopCorner(true);
            },
            this.showDelay
        ) as any;
    }

    public hide(): void {
        this.visibleAnimate = false;
        if (this.showTimeout) {
            clearTimeout(this.showTimeout);
        }
        this.hideTimeout = setTimeout(
            () => {
                this.visible = false;
                this.display = false;
            },
            this.hideDelay
        ) as any;
    }

    public onContainerClicked(event: MouseEvent): void {
        if (
            this.closeOnClick &&
            (event.target as HTMLElement).classList.contains("modal")
        ) {
            this.hide();
        }
    }

    public onMouseDown(event: MouseEvent): void {
        if (event.which !== 1) {
            return;
        }

        this.mouseMoveData = {
            inMove: false,
            x: event.pageX,
            y: event.pageY
        };
    }

    private windowWidth(): number {
        return window.innerWidth;
    }

    private windowHeight(): number {
        return window.innerHeight;
    }

    private dialogRect(): ClientRect {
        return this.dialog.nativeElement.getBoundingClientRect();
    }

    private dialogWidth(): number {
        return this.dialogRect().width;
    }

    private dialogHeight(): number {
        return this.dialogRect().height;
    }

    private setLeftTopCorner(force: boolean = false): void {
        if (this.changePosition || force) {
            this.left = Math.max(0, (this.windowWidth() - this.dialogWidth()) / 2);
            this.top = Math.max(0, (this.windowHeight() - this.dialogHeight()) / 2);
        }
    }

    @HostListener("document:mouseleave", ["$event"])
    private onDocumentMouseOut(event: Event): void {
        this.mouseMoveData = null;
    }

    @HostListener("document:mousemove", ["$event"])
    private onDocumentMouseMove(event: MouseEvent): boolean {
        if (!this.mouseMoveData) {
            return;
        }

        if (!this.mouseMoveData.inMove) {
            const moveX: number = event.pageX - this.mouseMoveData.x;
            const moveY: number = event.pageY - this.mouseMoveData.y;

            if (Math.abs(moveX) > 3 || Math.abs(moveY) > 3) {
                this.mouseMoveData.inMove = true;
            } else {
                return;
            }
        }

        let shiftX: number = event.pageX - this.mouseMoveData.x;
        let shiftY: number = event.pageY - this.mouseMoveData.y;

        if (this.left + this.dialogWidth() + shiftX > this.windowWidth() && shiftX > 0) {
            shiftX = Math.max(0, this.windowWidth() - (this.left + this.dialogWidth()));
        }
        if (this.left + shiftX < 0 && shiftX < 0) {
            shiftX = Math.min(0, -this.left);
        }
        this.left += shiftX;
        this.mouseMoveData.x += shiftX;

        if (this.top + this.dialogHeight() + shiftY > this.windowHeight() && shiftY > 0) {
            shiftY = Math.max(0, this.windowHeight() - (this.top + this.dialogHeight()));
        }
        if (this.top + shiftY < 0 && shiftY < 0) {
            shiftY = Math.min(0, -this.top);
        }
        this.top += shiftY;
        this.mouseMoveData.y += shiftY;

        return false;
    }

    @HostListener("document:mouseup", ["$event"])
    private onDocumentMouseUp(event: MouseEvent): void {
        this.mouseMoveData = null;
    }

    @HostListener("window:resize", ["$event"])
    private onWindowResize(event: Event): void {
        this.setLeftTopCorner();
    }
}
