import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";

import {ComponentModule} from "../component/component.module";
import {ModalDialogComponent} from "./modal/modal.dialog.component";

@NgModule({
    declarations: [ModalDialogComponent],
    exports: [ModalDialogComponent],
    imports: [CommonModule, ComponentModule]
})
export class DialogModule {
}
