import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {ComponentModule} from "./../component/component.module";

import {DialogModule} from "../dialog/dialog.module";
import {AppComponent} from "./app/app.component";
import {SharedLinkComponent} from "./sharedLink/sharedLink.component";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    declarations: [
        AppComponent,
        SharedLinkComponent
    ],
    imports: [
        BrowserModule,
        ComponentModule,
        DialogModule
    ]
})
export class AppModule {
}
