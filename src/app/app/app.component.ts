import {Component} from "@angular/core";

@Component({
    selector: "as-app",
    styleUrls: ["./app.component.styl"],
    templateUrl: "./app.component.html"
})
export class AppComponent {
}
