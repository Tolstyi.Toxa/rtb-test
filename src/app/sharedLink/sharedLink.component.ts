import {Component, HostListener, ViewChild} from "@angular/core";
import {EmailEditorComponent} from "../../component/emailEditor/emailEditor.component";
import {ModalDialogComponent} from "../../dialog/modal/modal.dialog.component";

@Component({
    selector: "as-component-shared-link",
    styleUrls: ["./sharedLink.component.styl"],
    templateUrl: "./sharedLink.component.html"
})
export class SharedLinkComponent {
    @ViewChild("dialog")
    public dialog: ModalDialogComponent;
    @ViewChild("emails")
    public emails: EmailEditorComponent;

    public showDialog(event: Event) {
        this.dialog.show();
    }

    public addEmail(event: Event): void {
        // А вдруг получим повторяющийся e-mail?
        while (this.emails.addContact(this.generateEmail()) !== 1) { }
    }

    public countEmail(event: Event): void {
        alert(this.emails.getContactsCount());
    }

    public closeDialog(event: Event) {
        this.dialog.hide();
    }

    private randomString(lenght: number): string {
        let res: string = "";
        const codebase: string = "abcdefghijklmnopqrstuvwxyz";

        for (let i = 0; i < lenght; ++i) {
            res += codebase[Math.round((codebase.length - 1) * Math.random())];
        }

        return res;
    }

    private generateEmail(): string {
        const nameLenght: number = 3 + Math.round(5 * Math.random());
        const domainLenght2: number = 5 + Math.round(3 * Math.random());
        const domainLenght1: number = 2 + Math.round(1 * Math.random());

        return `${this.randomString(nameLenght)}@${this.randomString(domainLenght2)}.${this.randomString(domainLenght1)}`;
    }

}
