/**
 * @author: Anton Shulgin
 */

import "@angular/common";
import "@angular/core";
import "@angular/http";
import "@angular/platform-browser";
import "@angular/platform-browser-dynamic";
import "@angular/router";

import "rxjs";

import * as $ from "jquery";
/* tslint:disable no-var-requires */
require("jquery-scrollstop");
